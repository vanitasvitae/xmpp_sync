package de.vanitasvitae.sync_client;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashSet;
import java.util.Set;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.SubscribeListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.jet.JetManager;
import org.jivesoftware.smackx.jingle.transport.jingle_ibb.JingleIBBTransportManager;
import org.jivesoftware.smackx.jingle.transport.jingle_s5b.JingleS5BTransportManager;
import org.jivesoftware.smackx.omemo.OmemoConfiguration;
import org.jivesoftware.smackx.omemo.OmemoManager;
import org.jivesoftware.smackx.omemo.exceptions.CorruptedOmemoKeyException;
import org.jivesoftware.smackx.omemo.provider.OmemoVAxolotlProvider;
import org.jivesoftware.smackx.omemo.signal.SignalOmemoService;
import org.jivesoftware.smackx.omemo.util.OmemoConstants;

import org.jxmpp.jid.FullJid;
import org.jxmpp.stringprep.XmppStringprepException;

public class Client {

    final XMPPConnection connection;
    final Path root;
    final Set<FullJid> remotes = new HashSet<>();

    public Client(String username, String password, String directory)
            throws XmppStringprepException, CorruptedOmemoKeyException, NoSuchAlgorithmException, UnsupportedEncodingException,
            InvalidKeyException, InterruptedException, XMPPException.XMPPErrorException, NoSuchPaddingException, BadPaddingException,
            InvalidAlgorithmParameterException, NoSuchProviderException, IllegalBlockSizeException, SmackException {
        File dir = new File(directory);
        if (!dir.exists()) {
            throw new IllegalArgumentException("Directory " + directory + " does not exist!");
        }

        root = Paths.get(directory);
        connection = new XMPPTCPConnection(username, password);
        Roster.getInstanceFor(connection).addSubscribeListener((from, subscribeRequest) -> SubscribeListener.SubscribeAnswer.Approve);
        JingleS5BTransportManager.getInstanceFor(connection);
        JingleIBBTransportManager.getInstanceFor(connection);

        JetManager jetm = JetManager.getInstanceFor(connection);

        SignalOmemoService.acknowledgeLicense();
        SignalOmemoService.setup();
        String userHome = System.getProperty("user.home");
        File storePath;
        if (userHome != null) {
            File f = new File(userHome);
            storePath = new File(f, ".config/omemo_store");
        } else {
            storePath = new File("omemo_store");
        }
        OmemoConfiguration.setFileBasedOmemoStoreDefaultPath(storePath);
    }

    void login() throws InterruptedException, IOException, SmackException, XMPPException, CorruptedOmemoKeyException {
        ((XMPPTCPConnection) connection).connect().login();
        connection.setReplyTimeout(30000);
        OmemoManager omemoManager = OmemoManager.getInstanceFor(connection);
        omemoManager.initialize();
        JetManager.getInstanceFor(connection).registerEnvelopeManager(OmemoManager.getInstanceFor(connection));
        JetManager.registerEnvelopeProvider(OmemoConstants.OMEMO_NAMESPACE_V_AXOLOTL, new OmemoVAxolotlProvider());
    }

    public void addRemote(FullJid remote) {
        remotes.add(remote);
    }
}
