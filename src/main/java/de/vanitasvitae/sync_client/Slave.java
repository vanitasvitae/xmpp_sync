package de.vanitasvitae.sync_client;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.hashes.element.HashElement;
import org.jivesoftware.smackx.jingle_filetransfer.JingleFileTransferManager;
import org.jivesoftware.smackx.jingle_filetransfer.component.JingleFile;
import org.jivesoftware.smackx.omemo.exceptions.CorruptedOmemoKeyException;

import org.jxmpp.stringprep.XmppStringprepException;

public class Slave extends Client {

    public Slave(String username, String password, String dir) throws XmppStringprepException,
            InvalidAlgorithmParameterException, NoSuchAlgorithmException, UnsupportedEncodingException,
            InvalidKeyException, InterruptedException, XMPPException.XMPPErrorException, NoSuchPaddingException,
            BadPaddingException, CorruptedOmemoKeyException, NoSuchProviderException, IllegalBlockSizeException,
            SmackException {
        super(username, password, dir);
        JingleFileTransferManager jingleFileTransferManager = JingleFileTransferManager.getInstanceFor(connection);
        jingleFileTransferManager.addIncomingFileOfferListener(offer -> {
            try {
                File target = new File(root.toFile(), offer.getMetadata().getName());
                if (target.exists() && target.length() == offer.getMetadata().getSize()) {

                    if (offer.getMetadata().getHashElement() != null) {
                        HashElement offerHash = offer.getMetadata().getHashElement();
                        try {
                            HashElement ourHash = JingleFile.calculateHash(target, offerHash.getAlgorithm());
                            if (Arrays.equals(ourHash.getHash(), offerHash.getHash())) {
                                System.out.println("We already seem to have file " + target.getAbsolutePath()+ ". Cancel.");
                                offer.cancel(connection);
                                return;
                            }
                        } catch (NoSuchAlgorithmException e) {
                            System.out.println("Unsupported hash algo: " + e);
                        }
                    }
                }

                target.getParentFile().mkdirs();
                offer.accept(connection, target);
            } catch (InterruptedException | XMPPException.XMPPErrorException | SmackException.NoResponseException | SmackException.NotConnectedException | IOException e) {
                e.printStackTrace();
            }
        });
    }
}
