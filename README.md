# GSoC 2017 - Jingle File Transfer Demo App
## File synchronisation via XMPP.

This application demonstratest the possibilities of Jingle File Transfer via XMPP. xmpp_sync can be used to mirror the contents of one directory across several computers.

### Building

Since some of the code I wrote for Smack is not yet merged, you'll have to build it yourselves.

```
git clone git@github.com:vanitasvitae/Smack.git
cd Smack
git checkout jingle3
gradle install
```

Next clone the repository of xmpp_sync.
```
cd ..
git clone git@github.com:vanitasvitae/xmpp_sync.git
cd xmpp_sync
gradle build
```

The resulting executable jar file can be found in `build/libs` afterwards.

### Available options and commands:

xmpp_sync has two modes: Master and Slave mode. A client started in master mode will send files
out, while clients in slave mode will receive files.

In order to use the program, you need at least two XMPP accounts (which might be on different
servers). I recommend using a server, which supports Socks5 proxies.

The application has the following options available when being started.
```
usage: client [-d] -D <arg> [-h] [-m] -p <swordfish> -u <user@server.tld>
 -d,--debug                        Print debug messages to the console.
 -D,--directory <arg>              Directory which will be synced.
 -h,--help                         Display this help text.
 -m,--master                       Start in master mode. When this flag is
                                   missing, the client will be started in
                                   slave mode.
 -p,--password <swordfish>         Password of the XMPP account.
 -u,--username <user@server.tld>   Barejid of the XMPP account.
```

So in order to start the application in master mode, you can use the following command:

`java -jar xmpp_sync.jar -u user@server.tld -p password123 -m -D master/directory`

If you want to start the application in slave mode instead, just leave out the `-m` flag.

There is a number of commands available to control the application once it is started.

```
 /help                             Display a help text
 /add <contact@otherserver.tld>    Add a contact to the contact list (roster).
 /trust <contact@otherserver.tld>  Trust a contacts OMEMO fingerprints.
 /sync <contact@otherserver.tld>   Add a contacts device to the sync list.
 /quit                             Stop the application.
```

Once you added a contact to your roster, you can add its devices to the sync list. Whenever you now drop a file into the master folder, it will be sent out OMEMO encrypted to all slave devices on the 
sync list. The OMEMO encryption is done using Smack's OMEMO capabilities, I implemented prior to GSoC and my [JET](https://geekplace.eu/xeps/xep-jet/xep-jet.html) specification which was part of my 
project.
